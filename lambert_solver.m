% LAMBERT PROBLEM SOLVER ALGORITHM BASED ON GIULIO AVANZINI APPROACH
%
%

function [vec_vel1, vec_vel2, error] = lambert_solver ...
                                       (x1, x2, t12_s, method, mu_cb)

r1 = sqrt(x1(1)^2+x1(2)^2+x1(3)^2);
r2 = sqrt(x2(1)^2+x2(2)^2+x2(3)^2);

tau12_s = t12_s*sqrt(mu_cb/(r1^3));
y_s = log(tau12_s);
vec_c = x2-x1;
c     = norm(vec_c);
	
e_f = (r1-r2)/c;
a_f = (r1+r2)/2;

p_f = a_f*(1-e_f^2);
	
% Define the true anomaly change to look for (depending on the transfer
% method)
if (method == 1)
    ver_h = cross(x1,x2);
    ver_h = ver_h/norm(ver_h);
    angle = acos(dot(x1,x2)/(r1*r2));
    delta_theta = angle;
    ver_t = cross(ver_h, vec_c);
    ver_t = ver_t/norm(ver_t);
    angle = acos(dot(x1,vec_c)/r1*c);
    delta_theta_c = angle;
elseif (method == -1)
    ver_h = cross(x2,x1);
    ver_h = ver_h/norm(ver_h);
    angle = acos(dot(x1,x2)/(r1*r2));
    delta_theta = 2*pi - angle;
    ver_t = cross(vec_c, ver_h);
    ver_t = ver_t/norm(ver_t);
    angle = acos(dot(x1,vec_c)/r1*c);
    delta_theta_c = 2*pi - angle;
end

% Compute the argument of pericentre of the fundamental ellipse with 
% respect to orbit reference frame with x = x1
angle = acos( dot(-vec_c,x1)/(c*r1));
if (method == 1)
    aop_f = -angle;
elseif (method == -1)
    aop_f = +angle;
end
% Set the limits the e_t
e_p = sqrt(1 - e_f^2);
e_h = sqrt(1/(cos(delta_theta/2))^2 - e_f^2);

% ---------------------- LAMBERT PROBLEM SOLUTION -------------------------
x = 0;
k = 0;

x_x= exp((1/e_h + 1/e_p)*x);

if (method == 1)
    e_t = e_p*(1 - exp(-x/e_p));
elseif (method == -1)
    e_t= e_p*e_h*(x_x-1)/(e_p+e_h*x_x);
end

e = sqrt(e_f^2 + e_t^2);

a = (p_f-e_t*r1*r2*sin(delta_theta)/c)/(1-e_f^2-e_t^2);

v1f = -aop_f;
v2f = -aop_f + delta_theta;

if (v2f > pi)
    v2f = v2f - 2*pi;
end

e_a1f = 2*atan2( sqrt((1-e_f)/(1+e_f))*sin(v1f/2), cos(v1f/2));
e_a2f = 2*atan2( sqrt((1-e_f)/(1+e_f))*sin(v2f/2), cos(v2f/2));

if (e_a2f < 0)
    e_a2f = 2*pi + e_a2f;
end

tau12 = (a/r1)^(1.5)*(e_a2f-e*sin(e_a2f)-e_a1f + e*sin(e_a1f));
y = log(tau12);

% Initializing the integration step
x = -e_p*log(1 - 0.001);
x_x = exp((1/e_h + 1/e_p)*x);

if (method == 1)
    e_ta = e_p*(1-exp(-x/e_p));
elseif(method == -1)
    e_ta = e_p*e_h*(x_x-1)/(e_p+e_h*x_x);
end

ea = sqrt(e_f^2 + e_ta^2);

aa = (p_f-e_ta*r1*r2*sin(delta_theta)/c)/(1-e_f^2-e_ta^2);

delta_aop = atan(e_ta/e_f);
v1a = v1f - delta_aop;

v2a = v2f - delta_aop;

if (v2a > pi) 
    v2a = v2a - 2*pi;
end

e_a1a = 2*atan2(sqrt((1-ea)/(1+ea))*sin(v1a/2),cos(v1a/2));
e_a2a = 2*atan2(sqrt((1-ea)/(1+ea))*sin(v2a/2),cos(v2a/2));  

if(e_a2a < 0)
    e_a2a = 2*pi+e_a2a;
end

tau12a=(aa/r1)^(1.5)*(e_a2a-ea*sin(e_a2a)-e_a1a+ea*sin(e_a1a));
ya = log(tau12a);

dy_dx = (ya-y)/x;
eps_x = (y_s-y)/(dy_dx);
eps_y =  y_s-y ;

max_iter = 100;
% Compute iterativelt the Lambert's problem solution
niter = 1;
v1 = v1a;
v2 = v2a;
while (abs(eps_y) > 1e-12 && abs(eps_x) > 1e-6 && niter < max_iter)
    k = k+1;
    x = x+eps_x;

    if (method == 1)

        e_t = e_p*(1-exp(-x/e_p));

    elseif(method == -1)

        x_x = exp((1/e_h + 1/e_p)*x);
        e_t = e_p*e_h*(x_x-1)/(e_p+e_h*x_x);
    end
    
    e = sqrt(e_f^2 + e_t^2);

    a = (p_f-e_t*r1*r2*sin(delta_theta)/c)/(1-e_f^2-e_t^2);

    delta_aop = atan(e_t/e_f);

    v1 = v1f - delta_aop;

    v2 = v2f - delta_aop;

    if(e < 1) 
        % elliptical transfers 
        e_a1 = 2*atan2(sqrt((1-e)/(1+e))*sin(v1/2),cos(v1/2));
        e_a2 = 2*atan2(sqrt((1-e)/(1+e))*sin(v2/2),cos(v2/2));

        if(e_a2 < 0)
            e_a2 = 2*pi+e_a2;
        end
        
        tau12=(a/r1)^(1.5)*(e_a2-e*sin(e_a2)-e_a1+e*sin(e_a1));

    elseif (e > 1)
        % Hyperbolic transfers
        j1 = tan(v1/2)*sqrt((e-1)/(e+1));
        j2 = tan(v2/2)*sqrt((e-1)/(e+1));
        h_a1 = log((1+j1)/(1-j1));
        h_a2 = log((1+j2)/(1-j2)); 
        
        coshf1 = (e+cos(v1))/(1+e*cos(v1));
        coshf2 = (e+cos(v2))/(1+e*cos(v2));
        if (v1 > 0 && v1 < pi)
            h_a1   = log(coshf1+sqrt(coshf1^2-1));
        else
            h_a1   = - log(coshf1+sqrt(coshf1^2-1));
        end
        if (v2 > 0 && v2 < pi)
            h_a2   = log(coshf2+sqrt(coshf2^2-1));
        else
            h_a2   = - log(coshf2+sqrt(coshf2^2-1));
        end

        tau12 = (-a/r1)^(1.5)*(e*sinh(h_a2)-h_a2-e*sinh(h_a1)+h_a1);

    end
    % Correct x for next iteration
    y_old = y;
    y = log(tau12);
    dy_dx = (y-y_old)/(eps_x);
    eps_y = (y_s-y);
    eps_x = eps_y/dy_dx;
    
    niter = niter + 1;
end

if (niter == max_iter)
    error = 'Convergence not reached';
else
    error = 'Solution found';
end
% Compute transfer orbit initial and final velocity
% Initial velocity vector
vel1 = sqrt(mu_cb*(2/r1-1/a));
phi1 = atan(e*sin(v1)/(1+e*cos(v1)));
h = vel1*cos(phi1)*r1;
vec_h = ver_h*h;
vel1_dot_x1 = cos(pi/2-phi1)*vel1*r1;
h_cross_x1 = cross(vec_h, x1);
vec_vel1 = (h_cross_x1 + vel1_dot_x1*x1)/(r1^2);

% Final velocity vector
vel2 = sqrt(mu_cb*(2/r2-1/a));
phi2 = atan(e*sin(v2)/(1+e*cos(v2)));
h = vel2*cos(phi2)*r2;
vec_h = ver_h*h;
vel2_dot_x2 = cos(pi/2-phi2)*vel2*r2;
h_cross_x2 = cross(vec_h, x2);
vec_vel2 = (h_cross_x2 + vel2_dot_x2*x2)/(r2^2);

vec_vel1 = vec_vel1';
vec_vel2 = vec_vel2';

return