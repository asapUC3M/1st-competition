# README #

This README is inteded to explain the contents of this repository

### What is this repository for? ###

* To keep all the information related to the 1st ASTROdynamics optiMIzation competitioN at UC3M (ASTROMIN)
* Version 1.0
* [More info](https://aero.uc3m.es)

### How do I get set up? ###

* The description of the problem is explained in detailed in ASTROMIN.pdf 
* The table of asteroids in cvs format can be found as Asteroid\_table.cvs
* You can find two matlab files that implement a Lambert solver. You can choose between them, which one fits better your needs
* Finally, you can download and check the documentation of GMAT from [here](https://gmat.gsfc.nasa.gov/)

### Guidelines Competition ###

* First, explore the solution space with a lambert solver 
* Second, establish a good reference trajectory with a high merit function J
* Third, rebuild the reference trajectory within GMAT
* You can share your doubts, and answer other team's questions at piazza.com/uc3m.es/spring2017/as1. The most collaborative team will have an special mention. 

### Who do I talk to? ###

* Register: send an email to manuel.sanjurjo@uc3m.es